# The groff command to run. Personally I prefer ms macros
# however you can change this to what you want.
GROFF = groff -ms

# The output format that groff will use.
# For example, to override this and generate a HTML document, you can use:
# `make OUTPUT=html`, and $(DOCUNAME).html should exist.
OUTPUT = pdf

# The name of the output document.
# For this example, it's "template"
DOCUNAME = template

.PHONY: all preview clean

all: $(DOCUNAME).$(OUTPUT)

# The main build rule.
# Calls groff with the output format
$(DOCUNAME).$(OUTPUT): src/$(DOCUNAME).ms
	$(GROFF) -T $(OUTPUT) $< > $@

# Preview allows you to look at the typeset document in terminal (it's quicker this way)
# Thank you -a flag for existing.
preview: src/$(DOCUNAME).ms
	@$(GROFF) -a $<

clean:
	-rm $(DOCUNAME).$(OUTPUT)
