.TL
A Small, But Sweet Template Document
.AU
Joseph Pence
.AI
\n[dy]-\n[mo]-\n[year]

.NH
Some section

.PP
Here's a paragraph. I'm going to type some useless things here, because nobody will read this.
Right? I'm hoping so, at least. But what do I know? Someone may spend their entire time reading
this filler content just for fun. I don't understand why. Here's a list of things, because why not. 
I don't have anything better to do, so why not waste time.
.nr step 1 1
.IP \n[step]. 3
.B "Item"
.IP \n+[step].
.B "Thing"
.IP \n+[step].
.B "Thingo"

And we continue the paragraph. This time, I die.
